import 'package:http/http.dart' as http;
import 'dart:convert' as json;

import 'model/movie.dart';

void main(List<String> arguments) async {
  print('Hello world!');

  var url =
      'https://api.themoviedb.org/3/movie/upcoming?api_key=469a6bfbc404b7247f5a56c48914f0a0&language=pt-BR';

  var response = await http.get(url);
  if (response.statusCode == 200) {
    var jsonMap = json.jsonDecode(response.body);
    //print(jsonMap);
    var list = jsonMap['results'].map((m) => Movie.fromJson(m)).toList();
    //var listMovie = list.map((e) => Movie.fromJson(e)).toList();

    list.forEach(
      (element) {
        print(element.title);
        print('Imagem: ${element.posterPath}');
      },
    );
  }
}
