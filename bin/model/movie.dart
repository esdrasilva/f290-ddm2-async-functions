class Movie {
  int id;
  String posterPath;
  double popularity;
  String title;
  var voteAverage;
  String overview;
  String releaseDate;

  Map<String, dynamic> toMap(Movie movie) {
    return {
      'id': movie.id,
      'poster_path': movie.posterPath,
      'popularity': movie.popularity,
      'title': movie.title,
      'vote_average': movie.voteAverage,
      'overview': movie.overview,
      'release_date': movie.releaseDate
    };
  }

  Movie.fromJson(Map<String, dynamic> map) {
    id = map['id'];
    posterPath = map['poster_path'];
    popularity = map['popularity'];
    title = map['title'];
    voteAverage = map['vote_average'];
    overview = map['overview'];
    releaseDate = map['release_date'];
  }

  @override
  String toString() {
    return '''
      Movie{id=$id, posterPath=$posterPath, popularity=$popularity, voteAverage=$voteAverage, overview=$overview, releaseDate; $releaseDate}
    ''';
  }
}
